from django.db import models
from django.conf import settings


class Posts(models.Model):
    nome = models.CharField(max_length=1000)
    data = models.IntegerField()
    text = models.CharField(max_length=1000)
    image_url = models.URLField(max_length=200, null=True)

    def __str__(self):
        return f'{self.nome} ({self.data})'

class Comentarios(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    data = models.DateTimeField(auto_now=True)
    post = models.ForeignKey(Posts, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Categorias(models.Model):
    nome = models.CharField(max_length=255)
    descri = models.CharField(max_length=255)
    posts = models.ManyToManyField(Posts)

    def __str__(self):
        return f'{self.nome}'
