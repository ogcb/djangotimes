from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Posts, Comentarios, Categorias
from django.shortcuts import render, get_object_or_404
from .forms import PostsForm, CommentsForm
from django.views import generic

class PostsDetailView(generic.DetailView):
    model = Posts
    context_object_name = 'post'
    template_name = 'times/detail.html'

class PostsListView(generic.ListView):
    model = Posts
    context_object_name = 'list_post'
    template_name = 'times/index.html'

def search_posts(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        list_post = Posts.objects.filter(nome__icontains=search_term)
        context = {"list_post": list_post}
    return render(request, 'times/search.html', context)

class PostsCreateView(generic.CreateView):
    def get(self, request, *args, **kwargs):
        context = {'form': PostsForm()}
        return render(request, 'times/create.html', context)

    def post(self, request, *args, **kwargs):
        form = PostsForm(request.POST)
        if form.is_valid():
            post = form.save()
            post.save()
            return HttpResponseRedirect(reverse('times:detail', args=[post.id]))
        return render(request, 'times/create.html', {'form': form})

class PostsUpdateView(generic.UpdateView):
    context_object_name = 'post'
    form_class = PostsForm
    model = Posts
    template_class = PostsForm
    template_name = "times/update.html"

    def get_success_url(self):
        return reverse('times:detail', kwargs={'pk': self.object.id})

class PostsDeleteView(generic.DeleteView):
    model = Posts
    context_object_name = 'post'
    template_name = 'times/delete.html'
    def get_success_url(self):
        return reverse('times:index')

def create_comment(request, post_id):
    post = get_object_or_404(Posts, pk=post_id)
    if request.method == 'POST':
        form = CommentsForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comentarios(author=comment_author,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('times:detail', args=(post_id, )))
    else:
        form = CommentsForm()
    context = {'form': form, 'post': post}
    return render(request, 'times/comment.html', context)

class CategoriesListView(generic.ListView):
    model = Categorias
    template_name = 'times/categories.html'

def categories_detail_list(request, category_id):
    category = get_object_or_404(Categorias, pk = category_id)
    context = {'category' : category}
    return render(request, 'times/categoriesdetail.html', context)
