from django.forms import ModelForm
from .models import Posts, Comentarios


class PostsForm(ModelForm):
        class Meta:
            model = Posts
            fields = [
                'nome',
                'data',
                'text',
                'image_url',
            ]
            labels = {
                'nome': 'Título',
                'data': 'Ano de Publicação',
                'text': 'Conteudo',
                'image_url': 'URL da Imagem',
        }

class CommentsForm(ModelForm):
        class Meta:
            model = Comentarios
            fields = [
                'author',
                'text',
            ]
            labels = {
                'author': 'Usuário',
                'text': 'Conteudo do comentario',
        }