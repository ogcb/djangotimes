from django.contrib import admin

from .models import Categorias, Posts, Comentarios

admin.site.register(Posts)
admin.site.register(Comentarios)
admin.site.register(Categorias)
# Register your models here.
