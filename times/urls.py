from django.urls import path
from . import views

app_name = 'times'
urlpatterns = [
    path('', views.PostsListView.as_view(), name='index'),
    path('search/', views.search_posts, name='search'),
    path('create/', views.PostsCreateView.as_view(), name='create'),
    path('<int:pk>/', views.PostsDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', views.PostsUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.PostsDeleteView.as_view(), name='delete'),
    path('<int:post_id>/comment/', views.create_comment, name='comment'),
    path('categories/', views.CategoriesListView.as_view(), name='categories'),
    path('categories/<int:category_id>/', views.categories_detail_list, name='categoriesdetail'),



]